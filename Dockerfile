FROM python:3.11-alpine

WORKDIR /

RUN apt-get update

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
EXPOSE 5000
CMD["python", "-u", "main.py"]