from flask import Flask
import os
import json

app = Flask(_name_)

# Load configuration from a JSON file
with open("config.json", "r") as config_file:
    config = json.load(config_file)

api_tk = config.get("API_TOKEN", "MySecureToken")
api_url = config.get("API_URL", "https://fakeweatherservice.com/getforecast")

@app.route("/one")
def one():
    return f'[{{"API_TOKEN":"{api_tk}","API_URL":"{api_url}"}}]'

@app.route("/")
def zero():
    return "Welcome Divya Abbireddy!"

if _name_ == "_main_":
    app.run(host="0.0.0.0", port=6001)
